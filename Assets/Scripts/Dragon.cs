﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dragon : MonoBehaviour {

    public GameObject particle;
    public GameObject projectile;
    public float speed;
    public int health;
    public float trustPower;

    Animator anim;    
    int dir;
    float dirTimer;
    float attackTimer;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        dirTimer = 0.0f;
        RandomDir();
    }
	
	// Update is called once per frame
	void Update () {
        Movement();
        Attack();
    }

    void RandomDir(bool needTurn = false)
    {
        if(!needTurn)
        {
            dir = Random.Range(0, 4);
        }
        else
        {
            int newDir;
            do
            {
                newDir = Random.Range(0, 4);
            }
            while (newDir == dir);
            
            dir = newDir;
        }
    }

    void Movement()
    {
        dirTimer += Time.deltaTime;
        if(dirTimer >= 0.7f)
        {
            dirTimer = 0.0f;
            RandomDir();
        }

        switch(dir)
        {
            case 0:
                transform.Translate(0, speed * Time.deltaTime, 0);
                anim.SetInteger("dir", 0);
                break;
            case 1:
                transform.Translate(0, -speed * Time.deltaTime, 0);
                anim.SetInteger("dir", 1);
                break;
            case 2:
                transform.Translate(-speed * Time.deltaTime, 0, 0);
                anim.SetInteger("dir", 2);
                break;
            case 3:
                transform.Translate(speed * Time.deltaTime, 0, 0);
                anim.SetInteger("dir", 3);
                break;
            default:
                break;
        }
    }

    void Attack()
    {
        attackTimer += Time.deltaTime;
        if(attackTimer >= 2.0f)
        {
            attackTimer = 0.0f;

            GameObject newProjectile = Instantiate(projectile, transform.position, transform.rotation);

            switch (dir)
            {
                case 0:
                    newProjectile.GetComponent<Rigidbody2D>().AddForce(Vector2.up * trustPower);
                    break;
                case 1:
                    newProjectile.GetComponent<Rigidbody2D>().AddForce(Vector2.down * trustPower);
                    break;
                case 2:
                    newProjectile.GetComponent<Rigidbody2D>().AddForce(Vector2.left * trustPower);
                    break;
                case 3:
                    newProjectile.GetComponent<Rigidbody2D>().AddForce(Vector2.right * trustPower);
                    break;
                default:
                    break;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Sword")
        {
            //Destroy(col.gameObject);
            col.GetComponent<Sword>().DestroyObjec(true);

            beAttack();
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<Player>().beAttack();

            beAttack();
        }
        else if (col.gameObject.tag == "Wall")
        {
            RandomDir(true);
        }
    }

    void beAttack()
    {
        --health;
        if (health <= 0)
        {
            Instantiate(particle, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}
