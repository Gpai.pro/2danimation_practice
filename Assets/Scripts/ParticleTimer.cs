﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleTimer : MonoBehaviour {

    public float destroyTimer;

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        destroyTimer -= Time.deltaTime;

        if(destroyTimer <= 0.0f)
        {
            Destroy(gameObject);
        }
    }
}
