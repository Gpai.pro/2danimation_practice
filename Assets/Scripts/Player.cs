﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour {
    
    public float speed;
    Animator anim;

    public int maxHealth;
    public Image[] hearts;
    int currentHealth;
    public GameObject sword;
    public float trustPower;
    public bool canMove;
    public bool canAttack;

    bool beInvicible;
    float invicibleTimer;
    SpriteRenderer spriteRender;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();

        if(!LoadGame())
            currentHealth = maxHealth;
        canMove = true;
        canAttack = true;
        showHeart();

        beInvicible = false;
        invicibleTimer = 0.0f;
        spriteRender = GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {        
        Movement();
        
        if (Input.GetKeyDown(KeyCode.X) && currentHealth < maxHealth)
        {
            ++currentHealth;
        }
        else if (Input.GetKeyDown(KeyCode.Z) && currentHealth > 0)
        {
            --currentHealth;
        }
        else if(Input.GetKeyDown(KeyCode.Space))
        {
            Attack();
        }
        showHeart();

        if(beInvicible)
        {
            invicibleTimer += Time.deltaTime;
            
            spriteRender.enabled = Random.Range(0, 2) == 0 ? true : false;
            if (invicibleTimer >= 1.0f)
            {
                beInvicible = false;
                spriteRender.enabled = true;
            }
        }
    }

    void showHeart()
    {
        for(int i =0; i < maxHealth; ++i)
        {
            if(i < currentHealth)
                hearts[i].gameObject.SetActive(true);
            else
                hearts[i].gameObject.SetActive(false);
        }
    }

    void Movement()
    {
        if (!canMove)
            return;

        anim.speed = 1;
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
        {
            transform.Translate(0, speed * Time.deltaTime, 0);
            anim.SetInteger("dir", 0);
        }
        else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
        {
            transform.Translate(0, -speed * Time.deltaTime, 0);
            anim.SetInteger("dir", 1);
        }
        else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            transform.Translate(-speed * Time.deltaTime, 0, 0);
            anim.SetInteger("dir", 2);
        }
        else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            transform.Translate(speed * Time.deltaTime, 0, 0);
            anim.SetInteger("dir", 3);
        }
        else
        {
            anim.speed = 0;
        }
    }

    void Attack()
    {
        if (!canAttack)
            return;

        canMove = false;
        canAttack = false;


        GameObject newSword = Instantiate(sword, transform.position, transform.rotation);
        if (currentHealth == maxHealth)
        {
            trustPower = 500;
            //canMove = true;
            //anim.SetInteger("attackDir", 5);
            newSword.GetComponent<Sword>().powerUp = true;
        }

        int dir = anim.GetInteger("dir");
        anim.SetInteger("attackDir", dir);
        
        if (dir == 0)
        {
            newSword.transform.Rotate(0, 0, 0);
            newSword.GetComponent<Rigidbody2D>().AddForce(Vector2.up * trustPower);
        }
        else if(dir == 1)
        {
            newSword.transform.Rotate(0, 0, 180);
            newSword.GetComponent<Rigidbody2D>().AddForce(Vector2.down * trustPower);
        }
        else if (dir == 2)
        {
            newSword.transform.Rotate(0, 0, 90);
            newSword.GetComponent<Rigidbody2D>().AddForce(Vector2.left * trustPower);
        }
        else if (dir == 3)
        {
            newSword.transform.Rotate(0, 0, -90);
            newSword.GetComponent<Rigidbody2D>().AddForce(Vector2.right * trustPower);
        }
    }

    public void beAttack()
    {
        if (beInvicible)
            return;

        --currentHealth;
        beInvicible = true;
        invicibleTimer = 0.0f;

        if(currentHealth <= 0)
        {
            SceneManager.LoadScene(0);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "EnemyBullet")
        {
            beAttack();

            col.GetComponent<Bullet>().Dead(true); ;
        }
        else if(col.gameObject.tag == "Potion")
        {
            //if (maxHealth >= 5 && currentHealth == maxHealth)
            //    return;

            if(maxHealth < 5)
                ++maxHealth;

            currentHealth = maxHealth;
            Destroy(col.gameObject);
        }
    }

    public void SaveGame()
    {
        PlayerPrefs.SetInt("maxHealth", maxHealth);
        PlayerPrefs.SetInt("currentHealth", currentHealth);
    }

    bool LoadGame()
    {
        if(PlayerPrefs.HasKey("maxHealth"))
        {
            maxHealth = PlayerPrefs.GetInt("maxHealth");
            currentHealth = PlayerPrefs.GetInt("currentHealth");
            return true;
        }
        else
            return false;
    }
}
