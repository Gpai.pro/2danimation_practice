﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public GameObject particle;
    float timer;

	// Use this for initialization
	void Start () {
        timer = 0.0f;

    }
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
		if(timer >= 1.0f)
        {
            Dead(false);
        }
	}

    public void Dead(bool playParticle)
    {
        if(playParticle)
        {
            Instantiate(particle, transform.position, transform.rotation);
        }

        Destroy(gameObject);
    }
}
