﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wizard : MonoBehaviour
{

    public GameObject particle;
    public GameObject projectile;
    public GameObject potion;
    public float speed;
    public int health;
    public float trustPower;

    Animator anim;
    int dir;
    float dirTimer;
    float attackTimer;
    float specialTimer;
    bool specialCD;

    Vector2[] spDir = new Vector2[8];

    // Use this for initialization
    void Start()
    {
        potion.SetActive(false);
        anim = GetComponent<Animator>();
        dirTimer = 0.0f;
        dir = 1;

        spDir[0] = new Vector2(0, 1);
        spDir[1] = new Vector2(1, 1);
        spDir[2] = new Vector2(1, 0);
        spDir[3] = new Vector2(1, -1);
        spDir[4] = new Vector2(0, -1);
        spDir[5] = new Vector2(-1, -1);
        spDir[6] = new Vector2(-1, 0);
        spDir[7] = new Vector2(-1, 1);        
    }

    // Update is called once per frame
    void Update()
    {
        specialTimer += Time.deltaTime;
        if(specialTimer >= 2.0f)
        {
            if(!specialCD)
            {
                SpecialAttck();
                anim.SetInteger("dir", dir);
                specialCD = true;
            }

            if(specialTimer >= 3.0f)
            {
                specialTimer = 0.0f;
                specialCD = false;
            }
        }
        else
        {
            Movement();
            //Attack();
        }
    }

    void ChangeDir()
    {
        switch (dir)
        {
            case 0:
                dir = 3;
                break;
            case 1:
                dir = 2;
                break;
            case 2:
                dir = 0;
                break;
            case 3:
                dir = 1;
                break;
            default:
                dir = 0;
                break;
        }
    }

    void Movement()
    {
        dirTimer += Time.deltaTime;
        if (dirTimer >= 1.5f)
        {
            dirTimer = 0.0f;
            ChangeDir();
        }

        switch (dir)
        {
            case 0:
                transform.Translate(0, speed * Time.deltaTime, 0);
                anim.SetInteger("dir", 0);
                break;
            case 1:
                transform.Translate(0, -speed * Time.deltaTime, 0);
                anim.SetInteger("dir", 1);
                break;
            case 2:
                transform.Translate(-speed * Time.deltaTime, 0, 0);
                anim.SetInteger("dir", 2);
                break;
            case 3:
                transform.Translate(speed * Time.deltaTime, 0, 0);
                anim.SetInteger("dir", 3);
                break;
            default:
                break;
        }
    }

    void Attack()
    {
        attackTimer += Time.deltaTime;
        if (attackTimer >= 1.0f)
        {
            attackTimer = 0.0f;            

            GameObject newProjectile = Instantiate(projectile, transform.position, transform.rotation);

            switch (dir)
            {
                case 0:
                    newProjectile.GetComponent<Rigidbody2D>().AddForce(Vector2.up * trustPower);
                    break;
                case 1:
                    newProjectile.GetComponent<Rigidbody2D>().AddForce(Vector2.down * trustPower);
                    break;
                case 2:
                    newProjectile.GetComponent<Rigidbody2D>().AddForce(Vector2.left * trustPower);
                    break;
                case 3:
                    newProjectile.GetComponent<Rigidbody2D>().AddForce(Vector2.right * trustPower);
                    break;
                default:
                    break;
            }
        }
    }

    void SpecialAttck()
    {
        foreach(Vector2 vec in spDir)
        {
            GameObject newProjectile = Instantiate(projectile, transform.position, transform.rotation);

            newProjectile.GetComponent<Rigidbody2D>().AddForce(vec * trustPower);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Sword")
        {
            //Destroy(col.gameObject);
            col.GetComponent<Sword>().DestroyObjec(true);

            beAttack();
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<Player>().beAttack();

            beAttack();
        }
        else if (col.gameObject.tag == "Wall")
        {
            ChangeDir();
        }
    }

    void beAttack()
    {
        --health;
        if (health <= 0)
        {
            potion.SetActive(true);
            Instantiate(particle, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}
