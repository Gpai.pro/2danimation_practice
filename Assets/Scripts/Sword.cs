﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour {

    float timer = 0.0f;
    const float powerUpTimer = 0.7f;
    const float normalTimer = 0.10f;
    public bool powerUp;
    public GameObject swordParticle;
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        if (timer >= normalTimer)
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>().SetInteger("attackDir", 5);
            GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().canMove = true;
        }

        if (powerUp)
        {
            if(timer >= powerUpTimer)
            {

                GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().canAttack = true;
                DestroyObjec(true);
            }
        }
        else
        {
            if (timer >= normalTimer)
            {
                GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().canAttack = true;
                DestroyObjec(false);
            }
        }
	}

    public void DestroyObjec(bool playParticle)
    {
        if (playParticle)
        {
            Instantiate(swordParticle, transform.position, transform.rotation);
        }

        GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>().SetInteger("attackDir", 5);
        GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().canMove = true;
        GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().canAttack = true;

        Destroy(gameObject);
    }

}
