﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crab : MonoBehaviour {

    public int health;
    public float speed;

    public GameObject particle;

    SpriteRenderer spriteRenderer;
    public Sprite faceUp;
    public Sprite faceDown;
    public Sprite faceLeft;
    public Sprite faceRight;

    float moveTimer = 1.5f;
    int direction = 1;    //0: up, 1: down, 2: right, 3: left

    // Use this for initialization
    void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();

        int dir = Random.Range(0, 4);
        setSpriteByDir(dir);
    }
	
	// Update is called once per frame
	void Update () {
        moveTimer -= Time.deltaTime;
        if(moveTimer <= 0)
        {
            moveTimer = 1.0f;
            int dir = Random.Range(0, 4);
            setSpriteByDir(dir);
        }

        Movement();
    }

    void setSpriteByDir(int dir)
    {
        if (dir == direction)
            return;

        direction = dir;

        switch(direction)
        {
            case 0:
                spriteRenderer.sprite = faceUp;
                break;
            case 1:
                spriteRenderer.sprite = faceDown;
                break;
            case 2:
                spriteRenderer.sprite = faceLeft;
                break;
            case 3:
                spriteRenderer.sprite = faceRight;
                break;
            default:
                break;
        }
    }

    void Movement()
    {
        switch (direction)
        {
            case 0:
                transform.Translate(0, speed * Time.deltaTime, 0);
                break;
            case 1:
                transform.Translate(0, -speed * Time.deltaTime, 0);
                break;
            case 2:
                transform.Translate(-speed * Time.deltaTime, 0, 0);
                break;
            case 3:
                transform.Translate(speed * Time.deltaTime, 0, 0);
                break;
            default:
                break;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Sword")
        {
            //Destroy(col.gameObject);
            col.GetComponent<Sword>().DestroyObjec(true);

            beAttack();
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<Player>().beAttack();

            beAttack();
        }
        else if(col.gameObject.tag == "Wall")
        {
            int dir;
            //moveTimer = 0.5f;
            switch(direction)
            {
                case 0:
                    dir = 1;
                    break;
                case 1:
                    dir = 0;
                    break;
                case 2:
                    dir = 3;
                    break;
                case 3:
                    dir = 2;
                    break;
                default:
                    dir = 0;
                    break;
            }
            setSpriteByDir(dir);
        }
    }

    void beAttack()
    {
        --health;
        if (health <= 0)
        {
            Instantiate(particle, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}
